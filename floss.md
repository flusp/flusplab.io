---
title: "O que é FLOSS?"
---

{% include add_ref.html id="freedoms" title="What is free software?" url="https://www.gnu.org/philosophy/free-sw.en.html" %}

Você já deve ter ouvido falar em projetos como o Linux, Gimp, Blender, Git ou GCC. Não só isso,
você provavelmente já usa software livre direta ou indiretamente no seu dia-a-dia. Produtos e
serviços como Android, Firefox, Smart TVs e até servidores web são ou dependem de software livre
para seu funcionamento. Todos estes projetos são considerados FLOSS: Free/Libre and Open Source
Software. O que isso quer dizer? Quer dizer que todo o código destes projetos estão disponíveis
para que qualquer um possa lê-los. Em outras palavras, é possível saber exatamente o que estes
softwares estão fazendo "por debaixo dos panos".  Existem várias vantagens de se ter projetos FLOSS
(que enumeraremos abaixo), mas primeiro precisamos definir exatamente o que é FLOSS/Software Livre.

### Software Livre

O termo Software Livre é uma tradução do termo Free Software, ou Libre Software. A escolha do termo
Libre, em inglês, foi usado para distinguir o uso da palavra *Free*.

> Free as in *freedom*, not as in *free beer*.

Mais formalmente, Software Livre designa qualquer pedaço de código que obedeça as quatro
liberdades {% include cite.html id="freedoms" %}.

0. A liberdade de executar o programa, para qualquer propósito;
1. A liberdade de estudar o programa, e adaptá-lo para as suas necessidades;
2. A liberdade de redistribuir cópias de modo que você possa ajudar ao seu próximo;
4. A liberdade de modificar o programa e distribuir tais modificações, de modo que toda a
   comunidade se beneficie.

Todo código que obedeça tais liberdades é dito livre. Estas liberdades garantem o livro uso do
conhecimento e de código, afim de que todos possam ajudar a melhorar o software e ajudar a
comunidade.

Note que nada nas quatro liberdades implicam na gratuidade do software. Inclusive é totalmente
válido que softwares livres sejam pagos, desde que ainda obedeçam as liberdades.

### open source software

open source e software livre se referem à mesma ideia. o uso deste ou daquele
termo depende do interesse do discurso: quando o assunto em pauta são questões
éticas, o termo mais usado é software livre. quando o assunto são questões
técnicas ou de mercado, o termo mais usado é open source ou código aberto. nós
utilizamos a expressão "floss" pois estamos interessados em ambas as pautas.

### Vantagens

As vantagens de software livre são muitas. Entre elas, temos:

- Compartilhamento de código com a comunidade;
- Maior interação para identificação e correção de falhas;
- Maior segurança para o usuário, já que a comunidade está constantemente contribuindo e examinando
    o código;
- Maior cuidado do contribuidor ao código do projeto, dado que seu código será revisado por seus
    colegas;
- Facilidade do desenvolvedor de integrar programas livres com seus próprios programas;

### Como contribuir?

Se você ficou interessado de contribuir para FLOSS, venha para o FLUSP! Atualmente, temos vários
contribuidores para projetos FLOSS de renome, como Linux, Git e GCC.

Você pode nos encontrar nos seguintes meios:

0. FLUSP Homepage: [https://flusp.ime.usp.br/](https://flusp.ime.usp.br/)
1. _Mailing list_: [https://groups.google.com/d/forum/flusp ](https://groups.google.com/d/forum/flusp)
4. IRC Server: **irc.freenode.net**. Channel: **#ccsl-usp**
5. Telegram: [https://t.me/FLUSP2](https://t.me/FLUSP2).
6. YouTube: [https://www.youtube.com/channel/UCYkMGyGL8h9vz06PcEjivWw](https://www.youtube.com/channel/UCYkMGyGL8h9vz06PcEjivWw)
7. Facebook: [https://www.facebook.com/flusp/](https://www.facebook.com/flusp/)

{% include print_bib.html %}
