---
title:  "EVAL-AD7780EBZ"
image:  "EVAL-AD7780EBZ"
ref: "resource"
categories: "resources.md"
id: 2
---

The EVAL-AD7780EBZ is a fully featured evaluation Board for the AD7780. The
evaluation board can be connected to a PC via a standard USB interface.
Software is provided enabling the user to perform detailed analysis of the
ADC's performance.

### URLs

* [EVAL-AD780](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/EVAL-AD7780.html)
* [User Guide](https://www.analog.com/media/en/technical-documentation/user-guides/UG-078.pdf)
